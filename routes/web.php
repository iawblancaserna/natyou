<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProductController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*===================================
                GENERAL
===================================*/


// HOME

// Route::get('/', function () {
//     return view('home');
// });

//Route::view('/', 'home');
Route::get('/', [HomeController::class, 'index'])->name('home.get');

// CONTACT US

Route::get('/contact', function () {
    return view('users/contact');
})->name('contact.get');

Route::post('/mail', [MailController::class, 'sendTest'])->name('contact.post');

// ABOUT US

Route::get('/about', function () {
    return view('about');
})->name('aboutUs');

// PRIVACY POLITY

Route::get('/policy', function () {
    return view('policy');
})->name('policy.get');

/*===================================
                USERS
===================================*/

// LOGIN

Route::get('/login', function () {
    return view('users/login');
})->name('login.get');

Route::post('/login', [LoginController::class, 'login'])->name('login.post');

Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

// REGISTER 

Route::get('/signup', function () {
    return view('users/signup');
})->name('signup.get');

Route::post('/signup', [RegisterController::class, 'register'])->name('register.post');

// Email Verification forgot password

Route::get('/forgetPassword', function () {
    return view('users/verification');
})->name('forgetPassword.get');

Route::post('/forgetPassword', [ForgotPasswordController::class, 'resetPassword'])->name('forgetPassword.post');


/*===================================
                PRODUCTS
===================================*/

Route::prefix('products')->group(function () {

    // SEARCH

    Route::get('/search', function () {
        return view('products/search');
    })->name('search.get');

    Route::post('/search', [ProductController::class, 'search'])->name('search.post');

    // ADD

    Route::get('/add', [ProductController::class, 'viewProdFormAdd'])->name('addProduct.get')->middleware('auth');

    Route::post('/add', [ProductController::class, 'create'])->name('createProduct');

    // View (list)

    Route::get('/list', [ProductController::class, 'list'])->name('listProducts');

    // EDIT

    Route::get('/edit/{id}', [ProductController::class, 'updateView'])->name('editProd.get')->middleware('auth');

    Route::post('/editProd', [ProductController::class, 'updatePost'])->name('editProd');

    // View (one product)

    Route::get('/view/{id}', [ProductController::class, 'viewOneProduct']);
});

/*===================================
                SHOP
===================================*/
Route::prefix('shops')->group(function () {

    // ADD

    Route::get('/add', function () {
        return view('shops/add');
    })->name('addShop.get')->middleware('auth');

    Route::post('/add', [ShopController::class, 'create'])->name('createShop');

    // EDIT
    Route::get('/edit/{id}', [ShopController::class, 'updateView'])->name('editShop.get')->middleware('auth');

    Route::post('/edit', [ShopController::class, 'updateShop'])->name('updateShop');

    // LIST
    Route::get('/list', [ShopController::class, 'list'])->name('listShop');
});


/*===================================
                CATEGORY
===================================*/

Route::get('/category/{type}', [CategoryController::class, 'listOneCat']);

/*===================================
                ADMIN               
===================================*/

Route::prefix('admin')->group(function () {

    // PRODUCTS LIST

    Route::get('/products', [ProductController::class, 'listAdmin'])->middleware('isAdmin');

    // DELETE PROD

    Route::get('/products/del/{id}', [ProductController::class, 'destroy'])->name('delete.prod')->middleware('isAdmin');

    // SHOPS LIST

    Route::get('/shops', [ShopController::class, 'listAdmin'])->name('shop.admin')->middleware('isAdmin');

    // DELETE SHOP

    Route::get('/shops/del/{id}', [ShopController::class, 'destroy'])->name('delete.shop')->middleware('isAdmin');

    // CATEGORY LIST

    Route::get('/categories', [CategoryController::class, 'list'])->middleware('isAdmin');

    // CREATE CATEGORY

    Route::get('/categories/create', function () {
        return view('admin/create');
    })->middleware('isAdmin');

    Route::post('/categories/create', [CategoryController::class, 'create'])->name('createCategory');

    // DELETE CATEGORY
    Route::get('/category/del/{id}', [CategoryController::class, 'delete'])->name('delete.cat');
});

// Auth::routes(); // ToDo

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
