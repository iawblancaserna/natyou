# NatYou

#### Laravel --> v9.9.0
#### Php --> v8.1.5
#### Mysql --> v8.0


## Diagrama de casos d'ús

![Diagrama de casos de uso](./doc/images/diagramas/diagramaCasosDeUso.png "Diagrama1")

## Diagrama BBDD

![Diagrama de base de dades](./doc/images/diagramas/diagramaBBDD.png "Diagrama2")

## Mockups

- Complements  

    - Side Menu

    ![Side Menu](./doc/images/mockups/sideMenu.png "Mockup")

    - Top Menu

    ![Top Menu](./doc/images/mockups/topMenu.png "Mockup")

- Views

    - Home

    ![Home](./doc/images/mockups/home.png "mockups")

    - Sign Up

    ![signUp](./doc/images/mockups/register.png "mockups")

    - Login

    ![login](./doc/images/mockups/login.png "mockups")

    - Password reset: verification email view

    ![Password reset 1](./doc/images/mockups/passwordReset1.png "mockups")
    
    - Password reset view

    ![Password reset 2](./doc/images/mockups/passwordReset2.png "mockups")

    - Product View

    ![Product](./doc/images/mockups/productView.png "mockups")

    - Product Add View 

    ![ProductAdd](./doc/images/mockups/newProduct.png "mockups")
    
    - Product Edit View 

    ![ProductEdit](./doc/images/mockups/editProduct.png "mockups")
    
    - Shop Add View

    ![ShopAdd](./doc/images/mockups/addShop.png "mockups")
    
    - Shop Edit View

    ![ShopEdit](./doc/images/mockups/editShop.png "mockups")

    - Category View

    ![Category](./doc/images/mockups/categoryView.png "mockups")

    - Search

    ![Search](./doc/images/mockups/search.png "mockups")

    - About Us

    ![About us](./doc/images/mockups/aboutUs.png "mockups")

    - Contact

    ![Contact](./doc/images/mockups/contact.png "mockups")

- Admin views

    - List product

    ![Admin 1](./doc/images/mockups/adminProductView.png "mockups")

    - Category add 
    
    ![Admin 2](./doc/images/mockups/adminCreateCategory.png "mockups")
    
    - Category List
    
    ![Admin 3](./doc/images/mockups/adminCategoryList.png "mockups")