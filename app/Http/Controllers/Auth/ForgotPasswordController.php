<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function resetPassword(Request $request)
    {
        // Si ha escrit el correu i no esta empty
        if (isset($request['email']) && !empty($request['email'])) {

            // Busquem que existeixi el usuari dins de la bd
            $user = User::where('email', '=', $request->email)->first();

            if ($user) {  // Si existeix
                $password = substr(md5(microtime()), 1, 6); // random pwd
                $user->password = bcrypt($password); // hash a la pwd
                $user->save(); // el guardem

                // Cridem a la funció que enviarà el correu a l'usuari
                $this->resetPasswordMail($user, $request->email, $password);
            } else {
                return view('users/verification')->with('msg', "We have not found this user.");
            }
        }
        return view('users/verification')
            ->with('msg', "Mail sent!!");
    }

    // Mail que se enviara
    public function resetPasswordMail($user, $email, $pwd)
    {

        $to_name = $user->name;
        $to_email = $email;
        $data = [
            'name' => $user->name,
            'surname' => $user->surname,
            'newPassword' => $pwd
        ];

        Mail::send('emails.recoverPwd', $data, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('NatYou - Recover Password');
            $message->from('naatyouu@outlook.com    ', 'NatYou');
        });
    }
}
