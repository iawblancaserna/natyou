<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->category = new Category();
    }

    //
    public function create(Request $request)
    {
        // Create new category
        $category = new Category();

        // Agafem dades del formulari
        $category->name = $request->cat;

        $category->save(); // la guardem a la bd

        $categories = DB::table('categories')->get(); // per pasar-ho a la vista (perq en el list te aquesta var)

        return view('admin/categories')
            ->with('categories', $categories);
    }


    // Listamos todas las categorias
    public function list() {
        
        $categories = DB::table('categories')->get();
        // dd($categories);

        return view('admin/categories')
            ->with('categories', $categories);
    }

    // Listamos una de las categorias (desde el sideMenu)
    public function listOneCat($type) {

        // Canviem variables per que corresponguin amb les vars a la bd
        // (a la ruta mai posem majuscules)
        if ($type == "withoutGluten") {
            $type = "Without Gluten";
        } else if ($type == "withoutOilPalm") {
            $type = "Without Oil Palm";
        } else if ($type == "freeCrueltyAnimal") {
            $type = "Free Cruelty Animal";
        } else if (($type == "vegan") || ($type == "vegetarian")) {
            $type = ucfirst($type);
        } 

        $cat = $this->category->query();
        $cat = $this->category->ByType($type)->first(); // FIRST - nomes retorna 1 (el get retorna array)

        $prods = $cat->products; // de la taula pivot

        return view('categories/list')
            ->with('prods', $prods)
            ->with('type', $type);
    }


    public function delete($id) {
        //
        $category = Category::find($id);
        $category->delete(); //returns true/false

        $categories = DB::table('categories')->get();

        return view('admin/categories')
            ->with('msg', 'Category deleted correctly.')
            ->with('categories', $categories);
    }
    
}
