<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendTest(ContactRequest $request) {
        $to_name = 'NatYou';
        $to_email = 'naatyouu@outlook.com';
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'msg' => $request->msg
        ];
        
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
            ->subject('Contact Us');
            $message->from('naatyouu@outlook.com', 'NatYou');
        });

        return view('users/contact')
            ->with('msg', 'Mail sent!');
    }
}
