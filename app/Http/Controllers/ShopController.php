<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShopRequest;
use App\Http\Requests\UpdateShopRequest;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isEmpty;

class ShopController extends Controller
{
    //

    public function create(ShopRequest $request)
    {

        if (!isset($request->lat)) {
            return view('shops/add')
                ->with('msg', 'You must mark the shop on the map!');
        }

        // Agafem dades del formulari
        Shop::create([ // la guardem a la bd
            'name' => $request->name,
            'address' => $request->address,
            'latitude' => $request->lat,
            'longitude' => $request->lng
        ]);

        $categories = $this->getCategories();

        return view('home')
            ->with('categories', $categories)
            ->with('msg', 'The shop was created successfully!');
    }

    public function list()
    {

        $shops = DB::table('shops')->get();

        return view('shops/list')
            ->with('shops', $shops)
            ->with('aux', 0);
    }

    public function updateView($id)
    {
        $shop = Shop::find($id);

        // try {
        //     $shop = Shop::find($id);
        // } catch (\Exception $e) {
        //     return view('error');
        // }

        return view('shops/edit')
            ->with('shop', $shop);
    }

    public function updateShop(UpdateShopRequest $request)
    {

        // $shop = Shop::find($request->id);


        Shop::where('id', $request->idShop)
            ->update([
                'name' => $request->name,
                'address' => $request->address,
                'latitude' => $request->lat,
                'longitude' => $request->lng
            ]);

        $categories = $this->getCategories();

        return view('home')
            ->with('categories', $categories)
            ->with('msg', 'The shop was updated successfully!');
    }


    // List all shops for the admin
    public function listAdmin()
    {
        $shops = DB::table('shops')->get();

        return view('admin/shops')
            ->with('shops', $shops);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Shop
     */
    public function destroy($id)
    {
        //
        $shop = Shop::find($id);
        $shop->delete(); //returns true/false

        $shops = DB::table('shops')->get();

        return view('admin/shops')
            ->with('msg', 'Shop deleted correctly.')
            ->with('shops', $shops);
    }

    // Quan es crea o s'edita una shop, retorna a la home
    // i la home necesita aquesta variable (pel sideMenu)
    public function getCategories()
    {

        $categories = DB::table('categories')->get();

        return $categories;
    }
}
