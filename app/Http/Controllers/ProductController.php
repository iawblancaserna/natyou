<?php

namespace App\Http\Controllers;

use App\Exceptions\UploadFileException;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->products = new Product();
    }

    /**
     * Show view with the form for create a product
     * 2 selects : un amb totes les shops i un amb totes categories
     */
    public function viewProdFormAdd()
    {

        // Agafem totes les categories 
        $categories = DB::table('categories')->get();

        // Agafem totes les shops 
        $shops = DB::table('shops')->get();

        return view('products/add')
            ->with('shops', $shops)
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(StoreProductRequest $request)
    {

        // Agafem les ids de les categories i shops
        $categoryIds = $request->get('categories');
        $shopIds = $request->get('shops');

        try {
            $file = $request->file('image');

            $filename = date('Y-m-d_H:i') . "_" . $file->getClientOriginalName();
            $file->move("img/products", $filename);

            // Create new product adding it to the db
            $product = Product::create([ // la guardem a la bd
                'name' => $request->name,
                'price' => $request->price,
                'description' => $request->msg,
                'image' => $filename
            ]);

            // Guardem a las M:N la info del prodXshop / prodXcat
            foreach ($categoryIds as $catId) {
                $product->categories()->attach($catId); // attach es per taules pivot
            }
            foreach ($shopIds as $shopId) {
                $product->shops()->attach($shopId);
            }
        } catch (UploadFileException $exception) {
            $this->error = $exception->customMessage();
        }

        $categories = $this->getCategories();

        return view('home')
            ->with('categories', $categories)
            ->with('msg', 'You created the product successfully!');
    }

    // List all the products (main page)
    public function list()
    {

        // ToDo --> get all the products i els mostrem a la vista
        $products = DB::table('products')->get();

        return view('products/list')
            ->with('products', $products);
    }

    // List all products for the admin
    public function listAdmin()
    {

        $products = DB::table('products')->get();

        return view('admin/products')
            ->with('products', $products);
    }

    // View one product info and map
    public function viewOneProduct($id)
    {

        // Busquem el producte en la bd amb la id pasada en la url
        $product = Product::find($id);

        $shops = $product->shops; // de la taula pivot
        $cats = $product->categories; // taula pivot

        return view('products/view')
            ->with('product', $product)
            ->with('shops', $shops)
            ->with('categories', $cats);
    }

    // Search product by name
    public function search(SearchRequest $request)
    {
        // Agafem el que ha escrit l'usuari
        $name = $request->input('prod');

        $listProducts = $this->products->query(); // comencem consulta sql

        $listProducts->byName($name);

        return view('products/search')
            ->with('products', $listProducts->get());
    }

    // View of update
    public function updateView($id)
    {
        // Agafem totes les categories 
        $categories = DB::table('categories')->get();
        $shops = DB::table('shops')->get();

        // Quin producte estem editant?
        $product = Product::find($id);

        // taules pivot
        // les agafem perque si no canvia res, volem que el value sigui el que tenia abans
        $shopsProd = $product->shops;
        $categoriesProd = $product->categories;

        $cats = [];
        // dd($categories);
        foreach ($categories as $key => $cat) {
            $selected = false;

            if ($categoriesProd->contains("id", $cat->id)) {
                $selected = true;
            }
            $cats[$key]['id'] = $cat->id;
            $cats[$key]['name'] = $cat->name;
            $cats[$key]['selected'] = $selected;
        }

        $allShops = [];
        foreach ($shops as $key => $shop) {
            $selected = false;

            if ($shopsProd->contains("id", $shop->id)) {
                $selected = true;
            }
            $allShops[$key]['id'] = $shop->id;
            $allShops[$key]['name'] = $shop->name;
            $allShops[$key]['selected'] = $selected;
        }
        //dd($shopsProd);
        return view('products/edit')
            ->with('shops', $allShops)
            ->with('product', $product)
            ->with('categories', $cats);
    }

    // Update and save update prod in db
    public function updatePost(Request $request)
    {

        $product = Product::find($request->id);

        // Agafem les ids de les categories i shops
        $categoryIds = $request->get('categories');
        $shopIds = $request->get('shops');

        // Si l'usuari ha pujat imatge
        if ($request->hasFile('image')) {
            // Intenta fer el update i moure/eliminar img de la carpeta
            try {
                // Quin file nou ha pujat
                $newFile = $request->file('image');

                $oldFilePath = $product->image;

                $filename = date('Y-m-d_H:i') . "_" . $newFile->getClientOriginalName();
                $newFile->move("img/products", $filename);

                // Borrar l'anterior foto
                unlink('img/products/' . $oldFilePath);

                // Update prod
                $product->update([
                    'name' => $request->name,
                    'description' => $request->msg,
                    'price' => $request->price,
                    'image' => $filename
                ]);

                // Fem un detach de cats i shops, per que no hi hagi duplicats en cas d'un canvi
                $product->categories()->detach();
                $product->categories()->detach();

                // Guardem a las M:N la info del prodXshop / prodXcat
                foreach ($categoryIds as $catId) {
                    $product->categories()->attach($catId); // attach es per taules pivot
                }
                foreach ($shopIds as $shopId) {
                    $product->shops()->attach($shopId);
                }
            } catch (UploadFileException $exception) {
                $this->error = $exception->customMessage();
            }
        } else {
            // Sinó, fes l'update sense la imatge
            $product->update([
                'name' => $request->name,
                'description' => $request->msg,
                'price' => $request->price,
            ]);

            // Fem un detach de cats i shops, per que no hi hagi duplicats en cas d'un canvi
            $product->categories()->detach();
            $product->categories()->detach();

            // Guardem a las M:N la info del prodXshop / prodXcat
            foreach ($categoryIds as $catId) {
                $product->categories()->attach($catId); // attach es per taules pivot
            }
            foreach ($shopIds as $shopId) {
                $product->shops()->attach($shopId);
            }
        }

        $categories = $this->getCategories();

        return view('home')
            ->with('categories', $categories)
            ->with('msg', 'The product was updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product = Product::find($id);
        $product->delete(); //returns true/false

        $products = DB::table('products')->get();


        // Borrem la fotografia que tenia a img/products
        $oldFilePath = $product->image;

        unlink('img/products/' . $oldFilePath);

        return view('admin/products')
            ->with('msg', 'Product deleted correctly.')
            ->with('products', $products);
    }

    // Quan es crea o s'edita un prod, retorna a la home
    // i la home necesita aquesta variable (pel sideMenu)
    public function getCategories()
    {
        $categories = DB::table('categories')->get();

        return $categories;
    }
}
