<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'surname' => 'required|max:100',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:5'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "The name is required",
            'surname.required' => "The surname is required",
            'email.required' => 'You have to specify your email',
            'email.unique' => 'This email is already registered',
            'password.required' => 'The password is required',
            'password.min' => 'The password can not be less than 5 characters.',
        ];
    }
}
