<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    // The model's default values for attributes.
    protected $fillable = [
        'name'
    ];

    
    // The products that belong to that category.
    public function products() {
        return $this->belongsToMany(Product::class, 'products_categories', 'idCategory', 'idProduct');
    }

    // SCOPES

    public function scopeByType($query, $type) {
        return $query->where('name', $type);
    }
}
