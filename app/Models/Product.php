<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    
    /**
    * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    // Indicates if the model should be timestamped.
    //public $timestamps = false;

    // The model's default values for attributes.
    protected $fillable = [
        'name',
        'price' ,
        'description',
        'image',
    ];

    // The categories that belong to the product.
    public function categories() {
        return $this->belongsToMany(Category::class, 'products_categories', 'idProduct', 'idCategory');
    }

    // The users that belong (created) that product
    public function users() {
        return $this->belongsToMany(User::class, 'users_products', 'idProduct', 'idUsers');
    }

    // The shops that belong (have) that product
    public function shops() {
        return $this->belongsToMany(Shop::class, 'products_shops', 'idProduct', 'idShop');
    }


    // SCOPES

    // Search by title
    public function scopeByName($query, $name) {
        return $query->where('name', 'like' ,'%'. $name .'%');

    }
}
