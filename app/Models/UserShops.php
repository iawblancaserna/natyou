<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserShops extends Pivot
{
    //
    protected $fillable = [
        'idUser',
        'idShop',
    ];

}
