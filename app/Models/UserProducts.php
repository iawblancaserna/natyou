<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserProducts extends Pivot
{
    //
    protected $fillable = [
        'idUser',
        'idProduct',
    ];

}
