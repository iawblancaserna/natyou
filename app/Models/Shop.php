<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shops';


    // Indicates if the model should be timestamped.
    //public $timestamps = false;

    // The model's default values for attributes.
    protected $fillable = [
        'name',
        'address',
        'latitude',
        'longitude'
    ];
    // The users that belong (created) that shop
    public function users()
    {
        return $this->belongsToMany(User::class, 'users_shops', 'idShop', 'idUser');
    }
}
