<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductShops extends Pivot
{
    //
    protected $fillable = [
        'idProduct',
        'idShop'
    ];

}
