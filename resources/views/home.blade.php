@extends('layouts.header')

@section('content')
    <header class="gradient-custom-3">
        @include('layouts.mainMenu', [
            'categories' => $categories,
        ])

        @include('layouts.sideMenu')
        <br> <br> <br>
    </header>
    <section class="gradient-custom-3 pt-5 pb-5" style="overflow-x: hidden;">
        <div class="container-fluid">
            <div class="row">
                @if (isset($msg))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{ $msg }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="carousel slide homeCarousel p-0" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{ asset('img/home/leopard.jpg') }}" class="d-block w-100" alt="leopard">
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('img/home/nature.jpg') }}" class="d-block w-100" alt="nature">
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('img/home/vegan3.jpg') }}" class="d-block w-100" alt="vegan">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <a href="{{ route('aboutUs') }}" class="text-decoration-none p-0">
                    <div class="col-12  imageBg" style="background-image: url({{ asset('img/home/forest.jpg') }})">
                        <div class="row text-absolute">
                            <div class="container text-center">
                                <p class="h2 title-new text-light">
                                    ABOUT US
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <br>

            <div class="row text-center">
                <div class="col-12 col-md-3 d-flex justify-content-center mb-1">
                    <div class="card border-0 text-white bg-dark" style="width: 25rem;">
                        <img src="{{ asset('img/home/heura.jpeg') }}" class="card-img-top" alt="markMap">
                        <div class="card-body text-center">
                            <a href="{{ route('addProduct.get') }}" class="button-30 linkMenu">Add Product</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 d-flex justify-content-center mb-1">
                    <div class="card border-0 text-white bg-dark" style="width: 25rem;">
                        <img src="{{ asset('img/home/noGluten.jpeg') }}" class="card-img-top" alt="noGluten">
                        <div class="card-body text-center">
                            <a href="{{ route('listProducts') }}" class="button-30 linkMenu">List Products</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 d-flex justify-content-center mb-1">
                    <div class="card border-0 text-white bg-dark" style="width: 25rem;">
                        <img src="{{ asset('img/home/googleMapsShop.png') }}" class="card-img-top" alt="phoneMaps">
                        <div class="card-body text-center">
                            <a href="{{ route('listShop') }}" class="button-30 linkMenu">List Shops</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 d-flex justify-content-center mb-1">
                    <div class="card border-0 text-white bg-dark" style="width: 25rem;">
                        <img src="{{ asset('img/home/mapsHome.jpg') }}" class="card-img-top" alt="markMap">
                        <div class="card-body text-center">
                            <a href="{{ route('createShop') }}" class="button-30 linkMenu">Add Shop</a>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <a href="{{ route('contact.get') }}" class="text-decoration-none p-0">
                    <div class="col-12  imageBg" style="background-image: url({{ asset('img/home/flowers.jpg') }})">
                        <div class="row text-absolute">
                            <div class="container text-center">
                                <p class="h2 title-new text-light">
                                    CONTACT US
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <br>
        </div>
    </section>
@endsection
