@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="gradient-custom-3 pt-5 pt-5">
        <div class="container">
            <div class="row text-center">
                <p class="h1">About us</p>
            </div>
            <br><br>
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="row">
                        <div class="col-6">
                            <img src="{{ asset('img/logos/palmOilFree.png') }}" alt="palmOilFree" class="img-fluid">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('img/logos/glutenFree.png') }}" alt="glutenFree" class="img-fluid">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 mt-4">
                            <img src="{{ asset('img/logos/freeCrueltyAnimal.png') }}" alt="freeCrueltyAnimal"
                                class="img-fluid">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('img/logos/vegan.png') }}" alt="vegan" class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-8 p-5 h3 textJustificat">
                    <br>
                    At NatYou we not only have a series of categories and products, but rather we promote a lifestyle.
                    <br>
                    A lifestyle that remained healthy, for us and for the planet. <br>
                    With this website we will start to get used to it.
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-12 col-md-6 h3 p-5 textJustificat">
                    Everyone is welcome here, whether it's someone who has never
                    tried a vegetarian diet, for example, or someone who has been following it for a long time.
                </div>
                <div class="col-12 col-md-6">
                    <img src="{{ asset('img/about/earth.jpg') }}" class="w-100" alt="earyh">

                </div>
            </div>
            <br> <br><br>
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{ asset('img/about/cosmos.jpg') }}" class="w-100" alt="cosmos">
                </div>
                <div class="col-12 col-md-6 p-5 h3 textJustificat">
                    We also want those people who may at the same time find and buy products, such as
                    people who they can't eat gluten, they can have it within reach in a more interactive and easy way.
                </div>
            </div>
            <br><br>
        </div>
    </section>
@endsection
