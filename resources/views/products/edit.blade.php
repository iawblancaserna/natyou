@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="vh-100 gradient-custom-3 pt-5">
        <div class="container">
            <div class="row">

            </div>
            <form action="{{ route('editProd') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row mt-5 mb-5 text-center">
                    <p class="h2">Editing the Product: <b>{{ $product->name }}</b></p>
                    <input type="hidden" value={{$product->id}} name="id">
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="name" class="h5">Product's name</label>
                                <input type="text" name="name" id="name" value="{{ $product->name }}"
                                    class="form-control form-control-lg mt-1" placeholder="Write the product name..">
                            </div>
                        </div>
                        <br>
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="shops" class="h5">Shop</label>
                                <select name="shops[]" multiple class="form-select form-select-lg mt-1">
                                    <option value="" selected disabled>Select where did you find the product</option>
                                    @foreach ($shops as $shop) 
                                        @if ($shop['selected'] == true)
                                            <option selected value="{{ $shop['id'] }}">{{ $shop['name'] }}</option>
                                        @else
                                            <option value="{{ $shop['id'] }}">{{ $shop['name'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="price" class="h5">Price</label>
                                <input type="text" name="price" id="price" value="{{ $product->price }}"
                                    class="form-control form-control-lg mt-1" placeholder="Product's price..">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="msg" class="h5">Description</label>
                                <textarea class="form-control form-control-lg mt-1" name="msg" id="msg" rows="3">{{ $product->description }}</textarea>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="form-group">
                                <label for="categories" class="h5">Category</label>
                                <select name="categories[]" multiple class="form-select form-select-lg mt-1">
                                    <option selected disabled>Which category is the product</option>
                                    @foreach ($categories as $cat)
                                        @if ($cat['selected'] == true)
                                            <option selected value="{{ $cat['id'] }}">{{ $cat['name'] }}</option>
                                        @else
                                            <option value="{{ $cat['id'] }}">{{ $cat['name'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="image" class="h5">Change the Image</label>
                                <input type="file" name="image" id="image" class="form-control form-control-lg mt-1">
                            </div>
                        </div>
                    </div>
                    <div class="row text-center mt-5 mb-5">
                        <div class="col-12">
                            <button type="submit" class="button-30"> Edit product</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
