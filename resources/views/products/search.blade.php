@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="vh-100 gradient-custom-3 pt-5">
        <div class="container">
            <form action="{{ route('search.post') }}" method="post">
                @csrf
                <div class="row text-center m-5">
                    <p class="h1">Search products</p> <br>
                </div>
                <div class="row">
                    <div class="row text-center mb-5">
                        <label for="prod" class="h4">What product do you want to search?</label>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-5 offset-md-3">
                            <input type="text" name="prod" id="prod" class="form-control form-control-lg">
                            <br>
                            @error('prod')
                                <div class="alert alert-info">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-12 col-md-4 mb-5">
                            <button type="submit" class="button-30"> Search </button>
                        </div>
                    </div>
            </form>
            <div class="row mt-5">
                @if (isset($products))
                    @forelse ($products as $prod)
                        <div class="card" style="width: 18rem; margin: 15px;">
                            <a href="/products/view/{{ urlencode($prod->id) }}">
                                <img class="card-img-top" src="{{ asset('img/products/' . $prod->image) }}"
                                    alt="Product-card image" width="286px" height="286px">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title text-center">{{ $prod->name }}</h5>
                            </div>
                        </div>
                    @empty
                        <div class="alert alert-warning text-center"> We haven't found a match.</div>
                    @endforelse
                @endif

            </div>
        </div>
    </section>
@endsection
