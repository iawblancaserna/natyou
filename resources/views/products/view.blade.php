@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="gradient-custom-3 pt-5">
        <div class="container pt-5">
            <div class="row">
                <div class="mb-3">
                    <div class="row g-0 text-left">
                        <div class="col-12 col-md-6">
                            <img src="{{ url('img/products/' . $product->image) }}" class="img-fluid">
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="card-body">
                                <p class="h1">{{ $product->name }}</p> <br>
                                <p class="card-text">{{ $product->description }}</p> <br>
                                <p class="card-text">Price --> {{ $product->price }}</p>

                                <p class="h5"><u>Shops</u></p>
                                @forelse ($shops as $item)
                                    <a class="link-primary" style="cursor: pointer;"
                                        onclick="initMap({{ $item->latitude }}, {{ $item->longitude }})">
                                        {{ $item->name }} | {{ $item->address }} <br>
                                    </a>
                                @empty
                                    No shops!
                                @endforelse
                                <br><br>
                                <p class="h5"><u>Categories</u></p>
                                @forelse ($categories as $item)
                                    {{ $item->name }} <br>
                                @empty
                                    No categories!
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Mapa con la tienda -->
            <div class="row">
                <div class="mt-5">
                    <div id="map"></div>
                </div>

                <script
                                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMfIgfrBlvcUvtSEhad7-QzgkQJHXDagQ&callback=initMap&v=weekly">
                </script>

                @include('complements.mapViewProd')

                <script>
                    // Quan es carrega la pagina, que surti la primera tenda per defecte
                    // Despres al clicar a las botigues anirà canviant

                    // Initialize lat and long
                    const lat = {{ $shops[0]->latitude }};
                    const long = {{ $shops[0]->longitude }};

                    // Cridem a la funcio
                    initMap(lat, long);
                </script>
            </div>
        </div>
    </section>
@endsection
