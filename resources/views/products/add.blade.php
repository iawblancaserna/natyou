@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="gradient-custom-3 pt-5">
        <div class="container">
            <div class="row">

            </div>
            <form action="{{ route('createProduct') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row mt-5 mb-5 text-center">
                    <p class="h2">Add new product</p>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="name" class="h5">Product's name</label>
                                <input type="text" name="name" id="name" class="form-control form-control-lg mt-1"
                                    placeholder="Write the product name.." value="{{ old('name') }}"> <br>
                                @error('name')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <br>
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="shops" class="h5">Shop</label>
                                <select name="shops[]" multiple class="form-select form-select-lg mt-1">
                                    <option value="" selected disabled>Select where did you find the product</option>
                                    @forelse ($shops as $shop)
                                        <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                                    @empty
                                        <option value="none" disabled>There aren't shops here.</option>
                                    @endforelse
                                </select> <br>
                                @error('shops')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="price" class="h5">Price</label>
                                <input type="text" name="price" id="price" class="form-control form-control-lg mt-1"
                                    placeholder="Product's price.." value="{{ old('price') }}"> <br>
                                @error('price')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="msg" class="h5">Description</label>
                                <textarea class="form-control form-control-lg mt-1" name="msg" id="msg" rows="3" placeholder="Product's description.."></textarea> <br>
                                @error('description')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="form-group">
                                <label for="categories" class="h5">Category</label>
                                <select name="categories[]" multiple class="form-select form-select-lg mt-1">
                                    <option selected disabled>Which category is the product</option>
                                    @forelse ($categories as $cat)
                                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                    @empty
                                        <option value="none" disabled>There aren't categories here.</option>
                                    @endforelse
                                </select> <br>
                                @error('categories')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="form-group">
                                <label for="image" class="h5">Image</label>
                                <input type="file" name="image" id="image" class="form-control form-control-lg mt-1"
                                    accept="image/jpeg,image/png,image/jpg"> <br>
                                @error('image')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row text-center mt-5 mb-5">
                        <div class="col-12">
                            <button type="submit" class="button-30"> Create product</button>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
