@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="gradient-custom-3 pt-5">
        <div class="container">
            <div class="row h1 text-center">
                <p> Product List </p>
            </div>
            <ul class="row list-group list-group-horizontal" style="justify-content: center;">
                @forelse ($products as $prod)
                    <div class="card" style="width: 18rem; margin: 15px; padding: 0px">
                        <a href="/products/view/{{ urlencode($prod->id) }}">
                            <img class="card-img-top" src="{{ asset('img/products/' . $prod->image) }}"
                                alt="Product-card image"  width="286px" height="286px">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title text-center">{{ $prod->name }}</h5>
                        </div>
                        <ul class="list-group list-group-flush text-center">
                            @auth
                                <a href="/products/edit/{{$prod->id}}" class="button-30 linkMenu">Edit Product</a>
                            @endauth
                        </ul>
                    </div>
                @empty
                    <div class="row mt-5">
                        <div class="col-12">
                            <div class="list-group">
                                <p class="list-group-item list-group-item-action text-center list-group-item-primary"
                                    aria-current="true">
                                    No products to show!
                                    If you want to create a product click <a href="/products/add"> here <a>
                                </p>
                                <br>
                            </div>
                        </div>
                    </div>
                @endforelse
            </ul>
        </div>
    </section>
@endsection
