@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="gradient-custom-3 pt-5 pb-5"> <br>
        <div class="container">
            <div class="row text-center">
                <p class="h1"> {{ ucfirst($type) }}</p>
            </div>
            <div class="row mt-5">
                @forelse ($prods as $prod)
                    <div class="card p-0" style="width: 18rem; margin: 15px;">
                        <a href="/products/view/{{ urlencode($prod->id) }}">
                            <img class="card-img-top" src="{{ asset('img/products/' . $prod->image) }}"
                                alt="Product-card image" width="286px" height="286px">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title text-center">{{ $prod->name }}</h5>
                        </div>
                    </div>
                @empty
                <div class="row mt-5">
                    <div class="col-12">
                        <div class="list-group">
                            <p class="list-group-item list-group-item-action text-center list-group-item-primary"
                                aria-current="true">
                                No products to show!
                                If you want to create a product click <a href="/products/add"> here <a>
                            </p>
                            <br>
                        </div>
                    </div>
                </div>
                @endforelse
            </div>

        </div>
    </section>
@endsection
