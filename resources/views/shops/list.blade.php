@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')

    <section class="gradient-custom-3 pt-5">
        <div class="container">
            <div class="row text-center">
                <p class="h2">Shop List</p>
            </div>
            <ul class="list-group" style="justify-content: center;">
                <ul class="row list-group list-group-horizontal" style="justify-content: center;">
                    @forelse ($shops as $shop)
                        <div hidden>{{ $aux++ }}</div>
                        <div class="card col-12 col-md-3 p-0" style="width: 500px; margin: 15px;">

                            <div class="shopMaps" id="map{{ $aux }}"></div> <!-- Mapa de la tenda-->

                            @include('complements.mapShopList')
                            <!-- js code of google maps api-->

                            <script>
                                // Asign the variables of lat and long
                                var latitude = {{ $shop->latitude }}
                                var longitude = {{ $shop->longitude }}
                                initMap(latitude, longitude); // cridem la funcio per carregar el mapa
                            </script>


                            <div class="card-body">
                                <h5 class="card-title text-center">{{ $shop->name }}</h5>
                                <li class="list-group-item"> {{ $shop->address }}</li>

                                @auth
                                    <li class="list-group-item text-center">
                                        <a href="/shops/edit/{{ $shop->id }}" class="button-30 linkMenu">Edit Shop</a>
                                    </li>
                                @endauth
                            </div>
                        </div>
                    @empty
                        <div class="row mt-5">
                            <div class="col-12">
                                <div class="list-group">
                                    <p class="list-group-item list-group-item-action text-center list-group-item-primary"
                                        aria-current="true">
                                        No shops to show!
                                        If you want to create a shop click <a href="/shops/add"> here <a>
                                    </p>
                                    <br>
                                </div>
                            </div>
                        </div>
                    @endforelse

                </ul>
            </ul>
        </div>
    </section>
@endsection
