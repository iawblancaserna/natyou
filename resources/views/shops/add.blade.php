@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="gradient-custom-3 pt-5" style="overflow-x: hidden;">
        <form id="newShop" action="{{ route('createShop') }}" method="post">
            @csrf
            <div class="container">
                <div class="row text-center mt-5 mb-5">
                    <p class="h1">Adding a shop</p>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-3">
                        <div class="form-group text-center">
                            <label for="name" class="h5">Shop's name:</label>
                            <input type="text" name="name" id="name" class="form-control form-control-lg"
                                placeholder="Write the shop's name here.." value="{{ old('name') }}">
                            <br>
                            @error('name')
                                <div class="alert alert-info">{{ $message }}</div>
                            @enderror
                        </div> <br>
                        <div class="form-group text-center">
                            <label for="address" class="h5">Address:</label>
                            <input type="text" name="address" id="address" class="form-control form-control-lg"
                                placeholder="Write the shop's address here.." value="{{ old('address') }}">
                            <br>
                            @error('address')
                                <div class="alert alert-info">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group mt-5">
                            <input type="hidden" type="text" name="lat" id="lat">
                        </div>
                        <div class="form-group mt-5">
                            <input type="hidden" type="text" name="lng" id="lng">
                        </div>
                        <div class="form-group mt-5 mb-5 text-center">
                            <button type="submit" class="button-30">Add shop</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row text-center pb-5">
                <label class="h3">Mark the location of the shop on the map!</label>
                <div class="col-4 offset-md-4">
                    @if (isset($msg))
                        <div class="alert-info alert">{{ $msg }}</div>
                    @endif
                </div>

                <div class="mt-5">
                    <div id="map"></div>
                </div>

            </div>

        </form>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
        @include('complements.shopMap')
    </section>
@endsection
