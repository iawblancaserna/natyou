<section>
    <nav class="navbar navbar-light justify-content-between bg-light menuTop">
        <a href="/"><img src="{{ asset('img/kat.png') }}" alt="logo" class="img-fluid logoMenu"></a>
        <div class="form-inline p-3 botons">
            @auth
                <form action="{{ Route('logout') }}" method="POST">
                    @csrf
                    <a href="/" class="linkMenu"><button class="button-30 m-2" type="button">Home</button></a>
                    {{-- Si es admin --}}
                    @if (auth()->user()->admin == 1)
                        <a href="/admin/products" class="linkMenu"><button class="button-30 m-2"
                                type="button">Admin</button></a>
                    @endif
                    <a href="#" class="linkMenu"><button class="button-30 m-2 buttonLogin"
                            type="submit">Logout</button></a>
                    <a href="{{route('search.get')}}"><i class="bi bi-search searchIcon"></i></a>
                </form>
            @endauth

            @guest
                <a href="/" class="linkMenu"><button class="m-2 button-30" type="button">Home</button></a>
                <a href="{{ route('signup.get') }}" class="linkMenu"><button class="button-30 m-2"
                        type="button">Sign Up</button></a>
                <a href="{{ route('login.get') }}" class="linkMenu"><button class="button-30 m-2 buttonLogin"
                        type="button">Login</button></a>
                <a href="{{ route('search.get') }}"><i class="bi bi-search searchIcon"></i></a>
            @endguest
        </div>
    </nav>
</section>
