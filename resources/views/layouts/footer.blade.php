<footer class="bg-dark text-white text-center text-lg-start">
    <div class="container p-4">
        <div class="row mt-5">
            <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                <h5 class="text-capitalize">You can join too!!</h5>
                <p class="text-justify">
                    At NatYou we not only have a series of categories and products, <br> but rather we promote a lifestyle.
                    <br>
                    A lifestyle that remained healthy, for us and for the planet. <br>
                    With this website we will start to get used to it.
                </p>
            </div>

            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase mb-0 pb-3"></h5>

                <ul class="list-unstyled pb-3">
                    <li class="pb-3">
                        <a href="{{route('contact.get')}}" class="text-white text-decoration-none">Contact</a>
                    </li>
                    <li class="pb-3">
                        <a href="{{route('home.get')}}" class="text-white text-decoration-none">Home</a>
                    </li>
                    <li class="pb-3">
                        <a href="{{route('signup.get')}}" class="text-white text-decoration-none">Sign up</a>
                    </li>
                    <li class="pb-3">
                        <a href="{{route('login.get')}}" class="text-white text-decoration-none">Login</a>
                    </li>
                </ul>
                
            </div>
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <img src="{{ asset('img/kat.png') }}" alt="logo" class="img-fluid logo">
                <br> <br>
                <p class="h4">&nbsp;&nbsp;NatYou</p>
            </div>
        </div>
    </div>

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        {{-- © --}}
        <img src="{{ asset('img/kgi.png') }}" alt="kgi" class="img-fluid" style="width: 35px">
        2022 Copyright: Blanca Serna Sanchez 
    </div>
</footer>
