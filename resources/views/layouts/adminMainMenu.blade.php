<section>
    <nav class="navbar navbar-light bg-light justify-content-between">
        <a href="/"><img src="{{ asset('img/kat.png') }}" alt="logo" class="img-fluid logoMenu"></a>
        <div class="form-inline p-3">
            <form action="{{ route('logout') }}" method="POST">
                <a href="/" class="btn btn-warning m-2" type="button">Home</a>
                <a href="/"><button class="btn btn-warning m-2" type="submit">Logout</button></a>
                <a href="/admin/products" class="btn btn-primary m-2" type="button">Products</a>
                <a href="/admin/categories" class="btn btn-primary m-2" type="button">Categories</a>
                <a href="{{route('shop.admin')}}" class="btn btn-primary m-2" type="button">Shops</a>
            </form>
        </div>
    </nav>
</section>
