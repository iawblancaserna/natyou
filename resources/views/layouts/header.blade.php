<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NatYou</title>

    <!-- Bootstrap link/script -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <!-- Bootstrap icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>

    <!-- CSS files -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- JS files -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Favicon x) -->
    <link rel="shortcut icon" href="{{ asset('img/kat.png') }}">

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

</head>


<body style="background-color: #a9de8e">
        @yield('content')
        <script type="text/javascript" charset="UTF-8">
            document.addEventListener('DOMContentLoaded', function () {
    
                cookieconsent.run({
                    "notice_banner_type":"simple",
                    "consent_type":"implied",
                    "palette":"light",
                    "language":"es",
                    "notice_banner_reject_button_hide":false,
                    "preferences_center_close_button_hide":false,
                    "page_refresh_confirmation_buttons":false,
                    "website_name":"NatYou",
                    "website_privacy_policy_url":"https://natyou.herokuapp.com/policy"
                });
            });
        </script>
        <script type="text/plain" cookie-consent="tracking">
            $(document).ready(function(){
                gtag('consent', 'update', {
                'analytics_storage': 'granted'
                });
                console.log('tracking fired');
            });
        </script>
        <script type="text/plain" cookie-consent="targeting">
            $(document).ready(function(){
                gtag('consent', 'update', {
                'ad_storage': 'granted'
                });
                console.log('targeting fired');
            });
        </script>
</body>
@include('layouts.footer')
<script>
    $(document).ready(function() {
        $(".burgerButton").click(function() {

            $(".sideMenu").toggle("slow", function() {
                if ($('.sideMenu').is(":visible")) {
                    $(".bi-list").addClass("white");
                } else {
                    $(".bi-list").removeClass("white");
                }
            });
            if ($(".sideMenu").hasClass("d-none")) {
                $('.sideMenu').removeClass('d-none');
            }
        });

        $(document).scroll(function() {
            $(".sideMenu").fadeOut();
            $(".bi-list").removeClass("white");
            $(".bi-list").addClass("black");
        });


    });
</script>

</html>
