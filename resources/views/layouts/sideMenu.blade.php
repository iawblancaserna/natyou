<nav class="nav flex-column bg-dark float-left sideMenu d-none">
    <div style=""></div>
    @forelse ($categories as $item)
        <a class="nav-link text-light menuLink" href="/category/{{$item->name}}"> {{ $item->name }}</a>
    @empty
        <div class="alert alert-info">No categories!</div>
    @endforelse
    {{-- <a class="nav-link text-light menuLink" href="/category/vegetarian">Vegetarian</a>
    <a class="nav-link text-light menuLink" href="/category/vegan">Vegan</a>
    <a class="nav-link text-light menuLink" href="/category/freeCrueltyAnimal">Free cruelty animal</a>
    <a class="nav-link text-light menuLink" href="/category/withoutGluten">Without gluten</a>
    <a class="nav-link text-light menuLink" href="/category/withoutOilPalm">Without oil palm</a> --}}
</nav>
<div>
    <button class="burgerButton"><i class="bi bi-list text-dark pl-5 burguer" style="z-index: 2;"></i></button>
</div>
