<script>
    function initMap(lat, long) {

        // all the locations that need to be marked (the shops)

        const myLatLng = {
            lat: lat,
            lng: long
        };

        // The map and its attributes
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 14,
            center: myLatLng,
        });

        // The marker
        const marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
        });
    }
    window.initMap = initMap;
</script>
