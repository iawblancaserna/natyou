@extends('layouts.headerAdmin')
@section('content')
    @include('layouts.adminMainMenu')
    <section class="adminSection">
        <div class="container">
            <div class="row pt-5 text-center">
                <p class="h2 text-light mb-5">Product list</p>
            </div>
            <div class="row pb-5 h5">
                <div class="col-12 col-md-8 offset-md-2">
                    @forelse ($products as $prod)
                        <input type="hidden" name="id" id="{{ $prod->id }}">
                        <ul class="list-group list-group-horizontal">
                            <li class="list-group-item list-group-item-primary flex-fill">{{ $prod->name }}</li>
                            <li class="list-group-item list-group-item-warning">
                                <a href="/admin/products/del/{{ urlencode($prod->id) }}">
                                    <i class="bi bi-x-square fs-4 text-warning pl-5"></i></a>
                            </li>
                        </ul>
                    @empty
                        <ul class="list-group list-group-horizontal">
                            <li class="list-group-item list-group-item-primary flex-fill">No products</li>
                        </ul>
                        <div class="col-12 col-md-2"></div>
                    @endforelse
                </div>
                <br>
                <br>
                @if (isset($msg))
                    <div class="alert alert-success"> {{ $msg }}</div>
                @endif
            </div>

    </section>
@endsection
