@extends('layouts.headerAdmin')
@section('content')
    @include('layouts.adminMainMenu')
    <section class="adminSection">
        <div class="container">
            <form action="{{ route('createCategory') }}" method="post">
                @csrf
                <div class="row text-center p-5 ">
                    <p class="h1 text-white">Create category</p>
                </div>
                <div class="row text-center">
                    <div class="col-12 col-md-6 offset-md-3">
                        <div class="form-group">
                            <label for="" class="h5 text-white">Category name you want to create: </label> <br>
                            {{-- <p class="card-text"><small class="text-white">Please use snake case for the names</small></p> --}}
                            <input type="text" name="cat" id="cat" class="form-control">
                        </div>
                    </div>
                    <div class="col-12 col-md-3"></div>
                </div>
                <div class="row text-center pt-5">
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning" >Create category</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
