@extends('layouts.headerAdmin')
@section('content')
    @include('layouts.adminMainMenu')
    <section class="adminSection">
        <div class="container">
            <div class="row pt-5 text-center">
                <p class="h2 text-light mb-5">Shop list</p>
            </div>
            <div class="row pb-5 h5">
                <div class="col-12 col-md-8 offset-md-2">
                    @forelse ($shops as $shop)
                        <input type="hidden" name="id" id="{{ $shop->id }}">
                        <ul class="list-group list-group-horizontal">
                            <li class="list-group-item list-group-item-primary flex-fill">{{ $shop->name }}</li>
                            <li class="list-group-item list-group-item-warning">
                                <a href="/admin/shops/del/{{ urlencode($shop->id) }}">
                                    <i class="bi bi-x-square fs-4 text-warning pl-5"></i></a>
                            </li>
                        </ul>
                    @empty
                        <ul class="list-group list-group-horizontal">
                            <li class="list-group-item list-group-item-primary flex-fill">No shops</li>
                        </ul>
                        <div class="col-12 col-md-2"></div>
                    @endforelse
                </div>
                <br>
                <br>
                <br>
                @if (isset($msg))
                    <div class="alert alert-success"> {{ $msg }}</div>
                @endif
            </div>

    </section>
@endsection
