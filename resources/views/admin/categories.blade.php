@extends('layouts.headerAdmin')
@section('content')
    @include('layouts.adminMainMenu')
    <section class="adminSection">
        <div class="container">
            <div class="row pt-5 text-center">
                <p class="h2 text-light mb-5">Category List</p>
            </div>
            <div class="row pb-5 h5">
                <div class="col-12 col-md-8 offset-md-2">
                    @if (!empty($categories))
                        @forelse ($categories as $cat)
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item list-group-item-primary flex-fill"> {{ $cat->name }}</li>
                                <li class="list-group-item list-group-item-warning">
                                    <a href="/admin/category/del/{{ urlencode($cat->id) }}"><i
                                            class="bi bi-x-square fs-4 text-warning pl-5"></i></a>
                                </li>
                            </ul>
                        @empty
                            <div class="row">
                                <div class="col-12 col-md-6 offset-3">
                                    <div class="list-group">
                                        <p class="list-group-item list-group-item-action text-center list-group-item-primary"
                                            aria-current="true">
                                            No categories to show!
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforelse
                    @endif

                </div>
                <div class="col-12 col-md-2"></div>
            </div>
            <div class="row text-center mt-5">
                <div class="col-12 col-md-8 offset-md-2">
                    @if (isset($msg))
                        <div class="alert alert-warning"> {{ $msg }}</div>
                    @endif
                </div>
                <div class="col-12 col-md-2"></div>

            </div>
            <div class="row">
                <div class="col-12 col-md-6 offset-md-3">
                    <div class="list-group">
                        <a href="/admin/categories/create" class="text-decoration-none"><button type="button"
                                class="list-group-item list-group-item-action text-center list-group-item-success"
                                aria-current="true">
                                Create a new category here
                            </button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
