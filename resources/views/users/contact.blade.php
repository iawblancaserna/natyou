@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="gradient-custom-3 pt-5 pb-5">
        <div class="container">
            <div class="row mb-5 text-center">
                <p class="h1">Do you have any question?<br> Fill out this form and we will answer you</p>
            </div>
            <div class="row">
                @if (isset($msg))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        {{ $msg }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <form action="{{ route('contact.post') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-12 form-group">
                                <label for="name" class="h3">Name:</label>
                                @auth
                                    <input type="text" name="name" id="name" class="form-control form-control-lg" value="{{auth()->user()->name}}"> <br>
                                @endauth
                                
                                @guest
                                    <input type="text" name="name" id="name" class="form-control form-control-lg"> <br>
                                @endguest

                                @error('name')
                                    <div class="alert-info alert">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-12 form-group">
                                <label for="name" class="h3">Email:</label>

                                @auth
                                    <input type="email" name="email" id="email" class="form-control form-control-lg" value="{{auth()->user()->email}}">
                                @endauth
                                
                                @guest
                                    <input type="email" name="email" id="email" class="form-control form-control-lg">   
                                @endguest


                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                    else.</small> <br>
                                @error('email')
                                    <div class="alert-info alert">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-12">
                                <label for="msg" class="h3">Description:</label>
                                <textarea class="form-control form-control-lg" name="msg" id="msg" cols="30" rows="10" placeholder="Write your message..."></textarea> <br>
                                @error('msg')
                                    <div class="alert-info alert">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group text-center  ">
                            <div class="col-12 col-md-6 offset-md-2 form-group">
                                <button class="button-30" type="submit" style="width: 100px"> Send </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-12 col-md-6">
                    <img src="{{asset('img/contact/contactCat.jpg')}}" alt="cat" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
@endsection
