@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="gradient-custom-3 pt-5 pb-5">
        <div class="container">
            <div class="row justify-content-center align-items-center h-100">
                <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                    <div class="card" style="border-radius: 15px;">
                        <div class="card-body p-5">
                            <h2 class="text-center mb-5">Sign Up</h2>
                            <form action="{{ route('register.post') }}" method="post">
                                @csrf

                                <div class="form-outline mb-4">
                                    <input type="text" name="name" id="name" class="form-control form-control-lg" />
                                    <label class="form-label" for="name">Your Name</label>
                                </div>
                                @error('name')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                                <div class="form-outline mb-4">
                                    <input type="text" name="surname" id="surname" class="form-control form-control-lg" />
                                    <label class="form-label" for="surname">Your Surname</label>
                                </div>
                                @error('surname')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                                <div class="form-outline mb-4">
                                    <input type="password" name="password" id="password"
                                        class="form-control form-control-lg" />
                                    <label class="form-label" for="form3Example4cg">Password</label>
                                </div>
                                @error('password')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                                <div class="form-outline mb-4">
                                    <input type="email" name="email" id="email" class="form-control form-control-lg" />
                                    <label class="form-label" for="form3Example4cdg">Your Email</label> <br>
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with
                                        anyone else.</small>
                                </div>
                                @error('email')
                                    <div class="alert alert-info">{{ $message }}</div>
                                @enderror
                                <div class="d-flex justify-content-center">
                                    <button type="submit" class="button-30">Register</button>
                                </div>
                                <p class="text-center text-muted mt-5 mb-0">Have already an account? <a
                                        href="{{ route('login.get') }}" class="fw-bold text-body"><u>Login here</u></a></p>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
