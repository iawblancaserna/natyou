@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="vh-100 gradient-custom-3 pt-5 pt-5">
        <div class="container">
            <form action="{{ route('forgetPassword.post') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-12 col-md-5 offset-md-3">
                        <div class="row text-center mb-5 mt-5">
                            <p class="h2">Password recovery</p>
                        </div>
                        <div class="row mt-5">
                            <div class="form-group">
                                <label class="h6" for="email">Introduce your email and we will send you a link
                                    for recover it!</label>
                                <input type="email" id="email" name="email" class="form-control form-control-lg">
                                @error('email')
                                    <div class="alert-info alert">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mt-3 text-center">
                                <button type="submit" class="button-30">Send</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row pt-5">
                    <div class="col-12 col-md-8 offset-md-2">
                        @if (isset($msg))
                            <div class="alert alert-info text-center">{{ $msg }}</div>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
