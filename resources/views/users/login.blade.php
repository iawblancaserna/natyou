@extends('layouts.header')
@section('content')
    @include('layouts.mainMenu')
    <section class="vh-100 gradient-custom-3 pt-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 d-flex align-items-center justify-content-center">
                    <div class="row">
                        <p class="h1">Login</p>
                        @error('errorLogin')
                            <div class="alert alert-info">
                                {{ $message }}
                            </div>
                        @enderror
                        <form action="{{ route('login.post') }}" method="post"> <br>
                            @csrf
                            <label for="email">Email:</label>
                            <input type="text" name="email" id="email" class="form-control form-control-lg"> <br>
                            @error('email')
                                <div class="alert alert-info">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="password">Password:</label>
                            <input type="password" name="password" id="password" class="form-control form-control-lg"> <br>
                            @error('password')
                                <div class="alert alert-info">
                                    {{ $message }}
                                </div>
                            @enderror
                            <a href="{{ route('forgetPassword.get') }}" class="text-primary">Have you forget your
                                password? Click here!</a>
                            <br> <br>
                            <button type="submit" value="login" id="login" class="button-30"
                                role="button">Login</button>
                        </form>
                        <br>
                        <div class="row pt-3">
                            <div class="col-12">
                                @if (isset($msg))
                                    <div class="alert alert-warning">
                                        {{ $msg }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 text-center">
                    <img class="img-fluid" style="margin-top: 115px;"
                        src="https://www.runtastic.com/blog/wp-content/uploads/2015/12/thumbnail_1200x800-1-1024x683.jpg"
                        alt="fruita">
                </div>
            </div>
        </div>
    </section>
@endsection
