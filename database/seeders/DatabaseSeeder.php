<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        
        // Usuari ADMIN

        DB::table('users')->insert([
            'email' => 'blancavel@gmail.com',
            'name' => 'Blancavel',
            'surname' => 'Serna',
            'password' => Hash::make('blanca'),
            'admin' => true
        ]);

        // SHOPS

        DB::table('shops')->insert([
            'name' => 'Mercadona Ronda Sant Pere',
            'address' => 'Ronda de Sant Pere, 31',
            'latitude' => '41.40819611980451',
            'longitude' => '2.1480846824950994'
        ]);

        DB::table('shops')->insert([
            'name' => 'Mercadona Paisos Catalans Mataró',
            'address' => 'Ronda dels Països Catalans, 11',
            'latitude' => '41.55276823413871',
            'longitude' => '2.4348652971315454'
        ]);

        DB::table('shops')->insert([
            'name' => 'Veritas Premià de Mar',
            'address' => 'Marina Port, Camí Ral, 139',
            'latitude' => '41.49084002394184',
            'longitude' => '2.368200345973017'
        ]);

        DB::table('shops')->insert([
            'name' => 'Aldi Premia de Mar',
            'address' => 'Port marina, S/N',
            'latitude' => '41.49123157277553',
            'longitude' => '2.367885965899803'
        ]);

        DB::table('shops')->insert([
            'name' => 'Glut End Rocafort',
            'address' => 'C. de Rocafort, 139',
            'latitude' => '41.382622589736066',
            'longitude' => '2.152657077429536'
        ]);

        DB::table('shops')->insert([
            'name' => 'Lidl El Masnou',
            'address' => 'Carrer GARBI, 2, 8',
            'latitude' => '41.484488934226086',
            'longitude' => '2.3392443104332994'
        ]);

        // // PRODUCTS

        // DB::table('products')->insert([
        //     'name' => 'Tofu',
        //     'description' => 'El tofu es una comida de origen oriental, preparada con semillas de soja, agua y solidificante o coagulante. Se basa en el valor nutritivo de la proteína de soja y en su capacidad para reducir el colesterol en sangre, entre otros beneficios.',
        //     'image' => '/productosSinSubir/tofu.jpg',
        //     'price' => 2.99
        // ]);

        // DB::table('products')->insert([
        //     'name' => "Bistec d'espinaques",
        //     'description' => 'Bistec de espinacas con gorgonzola',
        //     'image' => '/productosSinSubir/bistec.jpg',
        //     'price' => 3.99
        // ]);

        // DB::table('products')->insert([
        //     'name' => 'Falafel',
        //     'description' => 'Croquetas de garbanzos o habas.croqueta de garbanzos o habas. Libre de gluten!!',
        //     'image' => '/productosSinSubir/falafelSinGluten.png',
        //     'price' => 9.99
        // ]);

        // DB::table('products')->insert([
        //     'name' => 'Barritas',
        //     'description' => 'Barritas de cereales libres de gluten',
        //     'image' => '/productosSinSubir/barritas.jpg',
        //     'price' => 3.50
        // ]);

        // DB::table('products')->insert([
        //     'name' => 'Nocilla',
        //     'description' => 'Crema de chocolate con avellanas y sin aceite de palma',
        //     'image' => '/productosSinSubir/nocilla.png',
        //     'price' => 2.99
        // ]);

        // DB::table('products')->insert([
        //     'name' => 'Muffins',
        //     'description' => 'Magdalenas sin gluten',
        //     'image' => '/productosSinSubir/muffins.jpg',
        //     'price' => 4.56
        // ]);
    }
}
